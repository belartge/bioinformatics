import os


def get_available_num():
    files = os.listdir(os.getcwd())
    count = 0
    for x in files:
        if "test" in x:
            count += 1
    return count


def is_file_available(name):
    files_list = os.listdir(os.getcwd())
    print(name)
    if name in files_list:
        return True
    return False


def pick_the_file():
    available = get_available_num()
    print("Доступно", available, "файлов")
    if available > 0:
        file = input("Введите номер файла, начиная с 0: ")
        if is_file_available("test" + file + ".txt"):
            return "test" + file + ".txt"
        print("Ошибка")
    return ""


def naive_algorithm(input_line):
    """
    Наивный алгоритм поиска наибольшей грани
    """
    n = len(input_line)
    index = n - 1
    while index > 0:
        j = 0
        while j < index and input_line[j] == input_line[n - index + j]:
            j += 1
        if j == index:
            return index
        index -= 1
    return 0


def prefix_border_array(input_line):
    """
    Алгоритм поиска массива граней
    """
    n = len(input_line)
    bp = [0] * n
    for index in range(1, n):
        bp_right = bp[index - 1]
        while bp_right and input_line[index] != input_line[bp_right]:
            bp_right = bp[bp_right - 1]
        if input_line[index] == input_line[bp_right]:
            bp[index] = bp_right + 1
        else:
            bp[index] = 0
    return bp


def suffix_border_array(input_line):
    """
    Алгоритм поиска массива граней суффиксов
    """
    n = len(input_line)
    bs = [0] * n
    for i in range(n - 2, -1, -1):
        bs_left = bs[i + 1]  # позиция с конца слева от предыдущей грани
        while bs_left != 0 and (input_line[i] != input_line[n - bs_left - 1]):
            bs_left = bs[n - bs_left]
        if input_line[i] == input_line[n - bs_left - 1]:
            # длина на 1 больше, чем позиция
            bs[i] = bs_left + 1
        else:
            bs[i] = 0
    return bs


algo_num = -1
print(
    "Доступно 3 алгоритма:\n"
    "\t0. Наивный алгоритм поиска наибольшей грани\n"
    "\t1. Алгоритм поиска массива граней\n"
    "\t2. Алгоритм поиска массива граней суффиксов"
)
algos_available = range(0, 3)
while algo_num not in algos_available:
    print("Введите номер алгоритма")
    inputed = input(">")
    try:
        algo_num = int(inputed)
        if algo_num not in algos_available:
            raise IOError("Некорретный номер алгоритма")
    except:
        print("Введите корректный номер алгоритма")

algorithm = None
if algo_num == 0:
    algorithm = naive_algorithm
elif algo_num == 1:
    algorithm = prefix_border_array
elif algo_num == 2:
    algorithm = suffix_border_array

path = pick_the_file()

if path != "":
    file = open(path, "r").read()
    result = algorithm(file)
    print(result)
